﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="App.xaml.cs" company="evagapov">
//   Evgeny Agapov, 2016, CodeGeek audition test project.
// </copyright>
// <summary>
//   Defines the App.xaml type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionOOP.UI
{
    using System.Windows;

    /// <summary>
    /// The application.
    /// </summary>
    public partial class App : Application
    {
    }
}
