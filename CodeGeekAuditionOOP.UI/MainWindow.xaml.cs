﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="evagapov">
//   Evgeny Agapov, 2016, CodeGeek audition test project.
// </copyright>
// <summary>
//   Defines the MainWindow.xaml type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionOOP.UI
{
    using System.Windows;

    using CodeGeekAuditionOOP.Core.Car;
    using CodeGeekAuditionOOP.UI.ViewModels;

    /// <summary>
    /// The main window.
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Gets or sets the instance.
        /// </summary>
        private static MainWindow Instance { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            Instance = this;
        }

        /// <summary>
        /// Shows the <code>ManageCar</code> view.
        /// </summary>
        /// <param name="car">
        /// The car.
        /// </param>
        public static void ShowManageCarView(Car car)
        {
            if (Instance == null)
            {
                return;
            }

            Instance.CreateCarView.Visibility = Visibility.Collapsed;
            Instance.ManageCarView.Visibility = Visibility.Visible;

            Instance.ManageCarView.DataContext = new ManageCarViewModel(car);
        }
    }
}
