﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ManageCarView.xaml.cs" company="evagapov">
//   Evgeny Agapov, 2016, CodeGeek audition test project.
// </copyright>
// <summary>
//   Defines the ManageCarView.xaml type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Windows;
using System.Windows.Controls;

namespace CodeGeekAuditionOOP.UI.Views
{
    using CodeGeekAuditionOOP.Core.Details;
    using CodeGeekAuditionOOP.UI.ViewModels;

    /// <summary>
    /// Логика взаимодействия для ManageCarView.xaml
    /// </summary>
    public partial class ManageCarView : UserControl
    {
        public ManageCarView()
        {
            this.InitializeComponent();
        }

        private void TreeViewOnSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var vm = this.DataContext as ManageCarViewModel;
            var detail = e?.NewValue as Detail;

            if (vm != null)
            {
                vm.SelectedItem = detail;
            }
        }
    }
}
