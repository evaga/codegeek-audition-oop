﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreateCarView.xaml.cs" company="evagapov">
//   Evgeny Agapov, 2016, CodeGeek audition test project.
// </copyright>
// <summary>
//   Defines the CreateCarView.xaml type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace CodeGeekAuditionOOP.UI.Views
{
    using System.Windows.Controls;

    /// <summary>
    /// The <code>CreateCar</code> view.
    /// </summary>
    public partial class CreateCarView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateCarView"/> class.
        /// </summary>
        public CreateCarView()
        {
            this.InitializeComponent();
        }
    }
}
