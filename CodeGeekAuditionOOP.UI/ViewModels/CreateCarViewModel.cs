﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreateCarViewModel.cs" company="evagapov">
//   Evgeny Agapov, 2016, CodeGeek audition test project.
// </copyright>
// <summary>
//   Defines the CreateCarViewModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionOOP.UI.ViewModels
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Windows.Media;

    using CodeGeekAuditionOOP.Core.Car;
    using CodeGeekAuditionOOP.UI.Annotations;

    /// <summary>
    /// The <code>CreateCar</code> view model.
    /// </summary>
    public class CreateCarViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// The property changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The name.
        /// </summary>
        private string name;

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;

                this.NameColor = this.GetBrush(this.IsStringValid(value));
                this.OnPropertyChanged(nameof(this.Name));
            }
        }

        /// <summary>
        /// The name color.
        /// </summary>
        private Brush nameColor = Brushes.Black;

        /// <summary>
        /// Gets or sets the name color.
        /// </summary>
        public Brush NameColor
        {
            get
            {
                return this.nameColor;
            }

            set
            {
                this.nameColor = value;
                this.OnPropertyChanged(nameof(this.NameColor));
            }
        }

        /// <summary>
        /// The doors count.
        /// </summary>
        private string doorsCount;

        /// <summary>
        /// Gets or sets the doors count.
        /// </summary>
        public string DoorsCount
        {
            get
            {
                return this.doorsCount;
            }

            set
            {
                this.doorsCount = value;

                this.DoorsColor = this.GetBrush(this.IsNumberValid(value));
                this.OnPropertyChanged(nameof(this.DoorsCount));
            }
        }

        /// <summary>
        /// The doors color.
        /// </summary>
        private Brush doorsColor = Brushes.Black;

        /// <summary>
        /// Gets or sets the doors color.
        /// </summary>
        public Brush DoorsColor
        {
            get
            {
                return this.doorsColor;
            }

            set
            {
                this.doorsColor = value;
                this.OnPropertyChanged(nameof(this.DoorsColor));
            }
        }

        /// <summary>
        /// The wheels count.
        /// </summary>
        private string wheelsCount;

        /// <summary>
        /// Gets or sets the wheels count.
        /// </summary>
        public string WheelsCount
        {
            get
            {
                return this.wheelsCount;
            }

            set
            {
                this.wheelsCount = value;

                this.WheelsColor = this.GetBrush(this.IsNumberValid(value));
                this.OnPropertyChanged(nameof(this.WheelsCount));
            }
        }

        /// <summary>
        /// The wheels color.
        /// </summary>
        private Brush wheelsColor = Brushes.Black;

        /// <summary>
        /// Gets or sets the wheels color.
        /// </summary>
        public Brush WheelsColor
        {
            get
            {
                return this.wheelsColor;
            }

            set
            {
                this.wheelsColor = value;
                this.OnPropertyChanged(nameof(this.WheelsColor));
            }
        }

        /// <summary>
        /// Gets the create command.
        /// </summary>
        public RelayCommand CreateCommand
        {
            get
            {
                return new RelayCommand(this.Create);
            }
        }

        /// <summary>
        /// The <code>OnPropertyChanged</code> event handler.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Creates the car.
        /// </summary>
        /// <param name="obj">
        /// The object.
        /// </param>
        private void Create(object obj)
        {
            if (!this.IsStringValid(this.Name))
            {
                return;
            }

            if (!this.IsNumberValid(this.DoorsCount))
            {
                return;
            }

            if (!this.IsNumberValid(this.WheelsCount))
            {
                return;
            }

            MainWindow.ShowManageCarView(new Car(this.Name, int.Parse(this.DoorsCount), int.Parse(this.WheelsCount)));
        }

        /// <summary>
        /// Checks if string is valid.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> indicating whether string is valid.
        /// </returns>
        private bool IsStringValid(string value)
        {
            return !string.IsNullOrEmpty(value);
        }

        /// <summary>
        /// Checks if string is valid.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> indicating whether number is valid.
        /// </returns>
        private bool IsNumberValid(string value)
        {
            int parsed;
            return !string.IsNullOrEmpty(value) && int.TryParse(value, out parsed);
        }

        /// <summary>
        /// Gets the brush based on validation value.
        /// </summary>
        /// <param name="isValid">
        /// The value indicating whether the object is valid.
        /// </param>
        /// <returns>
        /// The <see cref="Brush"/>.
        /// </returns>
        private Brush GetBrush(bool isValid)
        {
            return isValid ? Brushes.Black : Brushes.Red;
        }
    }
}
