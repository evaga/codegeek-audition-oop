﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CarViewModel.cs" company="evagapov">
//   Evgeny Agapov, 2016, CodeGeek audition test project.
// </copyright>
// <summary>
//   Defines the CarViewModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionOOP.UI.ViewModels
{
    using System;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Globalization;
    using System.Runtime.CompilerServices;

    using CodeGeekAuditionOOP.Core;
    using CodeGeekAuditionOOP.Core.Car;
    using CodeGeekAuditionOOP.Core.Details;
    using CodeGeekAuditionOOP.UI.Annotations;

    /// <summary>
    /// The manage car view model.
    /// </summary>
    public class ManageCarViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// The property changed event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The selected door.
        /// </summary>
        private int? selectedDoor;

        /// <summary>
        /// Gets or sets the selected door.
        /// </summary>
        public string SelectedDoor
        {
            get
            {
                if (!this.selectedDoor.HasValue)
                {
                    return string.Empty;
                }

                return this.selectedDoor.Value.ToString(CultureInfo.InvariantCulture);
            }

            set
            {
                int parsed;
                if (int.TryParse(value, out parsed))
                {
                    this.selectedDoor = parsed;
                }
                else
                {
                    this.selectedDoor = null;
                }

                this.OnPropertyChanged(nameof(this.SelectedDoor));
            }
        }

        /// <summary>
        /// The details.
        /// </summary>
        private ObservableCollection<Detail> details;

        /// <summary>
        /// Gets or sets the details.
        /// </summary>
        public ObservableCollection<Detail> Details
        {
            get
            {
                return this.details;
            }

            set
            {
                this.details = value;
                this.OnPropertyChanged(nameof(this.Details));
            }
        }

        /// <summary>
        /// The messages.
        /// </summary>
        private string messages;

        /// <summary>
        /// Gets or sets the messages
        /// </summary>
        public string Messages
        {
            get
            {
                return this.messages;
            }

            set
            {
                this.messages = value;
                this.OnPropertyChanged(nameof(this.Messages));
            }
        }

        /// <summary>
        /// Gets or sets the name of the car.
        /// </summary>
        public string Name
        {
            get
            {
                return this.Car == null ? string.Empty : this.Car.Name;
            }
        }

        /// <summary>
        /// Gets or sets the selected item.
        /// </summary>
        public Detail SelectedItem { get; set; }

        /// <summary>
        /// Gets the <code>Add</code> command.
        /// </summary>
        public RelayCommand AddCommand
        {
            get
            {
                return new RelayCommand(this.Add);
            }
        }

        /// <summary>
        /// Gets the <code>Calculate</code> command.
        /// </summary>
        public RelayCommand CalculateCommand
        {
            get
            {
                return new RelayCommand(this.Calculate);
            }
        }

        /// <summary>
        /// Gets the <code>Open</code> command.
        /// </summary>
        public RelayCommand OpenCommand
        {
            get
            {
                return new RelayCommand(this.Open);
            }
        }

        /// <summary>
        /// Gets the <code>Remove</code> command.
        /// </summary>
        public RelayCommand RemoveCommand
        {
            get
            {
                return new RelayCommand(this.Remove);
            }
        }

        /// <summary>
        /// Gets the <code>Rotate</code> command.
        /// </summary>
        public RelayCommand RotateCommand
        {
            get
            {
                return new RelayCommand(this.Rotate);
            }
        }

        /// <summary>
        /// Gets or sets the car.
        /// </summary>
        private Car Car { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Car"/> class.
        /// </summary>
        public ManageCarViewModel()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Car"/> class.
        /// </summary>
        /// <param name="car">
        /// The car.
        /// </param>
        public ManageCarViewModel(Car car)
        {
            this.Car = car;
            this.Details = car.Details;

            Output.Instance.Written += (sender, agrs) => { this.Messages = $"{agrs.Message}{this.Messages}"; };
        }
        
        /// <summary>
        /// The <code>OnPropertyChanged</code> event handler.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Ads a new detail.
        /// </summary>
        /// <param name="obj">
        /// The object.
        /// </param>
        private void Add(object obj)
        {
            var dialog = new SelectTypeDialog(this.SelectedItem is Wheel);
            if (dialog.ShowDialog() == true)
            {
                if (dialog.SelectedType == typeof(Door))
                {
                    this.Details.Add(new Door());
                }
                else if (dialog.SelectedType == typeof(Wheel))
                {
                    this.Details.Add(new Wheel());
                }
                else if (dialog.SelectedType == typeof(Nut))
                {
                    this.SelectedItem.Details.Add(new Nut());
                }
            }
        }

        /// <summary>
        /// Opens the door.
        /// </summary>
        /// <param name="obj">
        /// The object.
        /// </param>
        private void Open(object obj)
        {
            if (string.IsNullOrEmpty(this.SelectedDoor))
            {
                Output.WriteLine(@"Необходимо указать номер двери");
                return;
            }

            try
            {
                this.Car?.Open(int.Parse(this.SelectedDoor));
            }
            catch (InvalidOperationException)
            {
                Output.WriteLine($"Не удалось найти дверь №{this.SelectedDoor}");
            }
        }
        
        /// <summary>
        /// Removes detail.
        /// </summary>
        /// <param name="obj">
        /// The object.
        /// </param>
        private void Remove(object obj)
        {
            if ((this.SelectedItem != null) && !(this.SelectedItem is Body))
            {
                this.Car.RemoveDetail(this.SelectedItem);
            }
        }

        /// <summary>
        /// Rotates the car.
        /// </summary>
        /// <param name="obj">
        /// The object.
        /// </param>
        private void Rotate(object obj)
        {
            this.Car?.Rotate();
        }

        /// <summary>
        /// Calculates the weight of a car.
        /// </summary>
        /// <param name="obj">
        /// The object.
        /// </param>
        private void Calculate(object obj)
        {
            this.Car?.CalculateWeight();
        }
    }
}