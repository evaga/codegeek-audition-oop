﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SelectTypeDialog.xaml.cs" company="evagapov">
//   Evgeny Agapov, 2016, CodeGeek audition test project.
// </copyright>
// <summary>
//   Defines the SelectTypeDialog.xaml type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionOOP.UI
{
    using System;
    using System.Windows;

    using CodeGeekAuditionOOP.Core.Details;

    /// <summary>
    /// The select type dialog.
    /// </summary>
    public partial class SelectTypeDialog : Window
    {
        /// <summary>
        /// Gets or sets the selected type.
        /// </summary>
        public Type SelectedType { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SelectTypeDialog"/> class.
        /// </summary>
        /// <param name="isWheel">
        /// The value indicating the dialog is open for wheel.
        /// </param>
        public SelectTypeDialog(bool isWheel)
        {

            this.InitializeComponent();
            this.Loaded += (sender, args) =>
            {
                this.DoorButton.IsEnabled = !isWheel;
                this.WheelButton.IsEnabled = !isWheel;
                this.NutButton.IsEnabled = isWheel;
            };
        }

        /// <summary>
        /// The door button <code>OnClick</code> event handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        private void DoorButtonOnClick(object sender, RoutedEventArgs e)
        {
            this.SelectedType = typeof(Door);
            this.DialogResult = true;
        }
        
        /// <summary>
        /// The door button <code>OnClick</code> event handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        private void WheelButtonOnClick(object sender, RoutedEventArgs e)
        {
            this.SelectedType = typeof(Wheel);
            this.DialogResult = true;
        }
        
        /// <summary>
        /// The nut button <code>OnClick</code> event handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        private void NutButtonOnClick(object sender, RoutedEventArgs e)
        {
            this.SelectedType = typeof(Nut);
            this.DialogResult = true;
        }
        
        /// <summary>
        /// The cancel button <code>OnClick</code> event handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        private void CancelButtonOnClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
