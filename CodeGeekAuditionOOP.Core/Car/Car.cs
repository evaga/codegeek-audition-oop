﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Car.cs" company="evagapov">
//   Evgeny Agapov, 2016, CodeGeek audition test project.
// </copyright>
// <summary>
//   Defines the Car.cs type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionOOP.Core.Car
{
    using System.Collections.ObjectModel;
    using System.Linq;

    using CodeGeekAuditionOOP.Core.Details;

    /// <summary>
    /// The car.
    /// </summary>
    public class Car : DetailsCollector
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the details of car.
        /// </summary>
        public override sealed ObservableCollection<Detail> Details { get; protected set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Car"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="doorsCount">
        /// The doors count.
        /// </param>
        /// <param name="wheelsCount">
        /// The wheels count.
        /// </param>
        public Car(string name, int doorsCount, int wheelsCount)
        {
            this.Name = name;
            this.Details = new ObservableCollection<Detail>(new Detail[] { new Body() }
                .Concat(Enumerable.Range(1, doorsCount).Select(i => new Door()))
                .Concat(Enumerable.Range(1, wheelsCount).Select(i => new Wheel()))
                .ToList());
        }

        /// <summary>
        /// Rotates all rotatable objects.
        /// </summary>
        public void Rotate()
        {
            this.Details.OfType<IRotatable>().ToList().ForEach(detail => detail.Move());
        }

        /// <summary>
        /// Opens the door object.
        /// </summary>
        /// <param name="number">
        /// The number of the door object.
        /// </param>
        public void Open(int number)
        {
            this.Details.OfType<IDoor>().First(door => door.Number == number).Open();
        }

        /// <summary>
        /// Gets the weight of the car.
        /// </summary>
        public void CalculateWeight()
        {
            Output.WriteLine($"Вес машины: {this.Details.Sum(detail => detail.GetSumWeight()):G4}кг");
        }
    }
}
