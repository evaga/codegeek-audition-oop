﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DetailsCollector.cs" company="evagapov">
//   Evgeny Agapov, 2016, CodeGeek audition test project.
// </copyright>
// <summary>
//   Defines the DetailsCollector.cs type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionOOP.Core
{
    using System.Collections.ObjectModel;

    using CodeGeekAuditionOOP.Core.Details;

    /// <summary>
    /// The details collector.
    /// </summary>
    public abstract class DetailsCollector
    {
        /// <summary>
        /// Gets or sets the details encapsulated by collector.
        /// </summary>
        public virtual ObservableCollection<Detail> Details
        {
            get
            {
                return new ObservableCollection<Detail>();
            }

            protected set
            {
            }
        }

        /// <summary>
        /// Adds the detail.
        /// </summary>
        /// <param name="detailToAdd">
        /// The detail to add.
        /// </param>
        public void AddDetail(Detail detailToAdd)
        {
            this.Details.Add(detailToAdd);
        }

        /// <summary>
        /// Removes the child detail recursively.
        /// </summary>
        /// <param name="detailToRemove">
        /// The detail to remove.
        /// </param>
        public void RemoveDetail(Detail detailToRemove)
        {
            if (this.Details.Contains(detailToRemove))
            {
                this.Details.Remove(detailToRemove);
                return;
            }

            foreach (var detail in this.Details)
            {
                detail.RemoveDetail(detailToRemove);
            }
        }
    }
}
