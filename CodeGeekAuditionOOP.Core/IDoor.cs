﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDoor.cs" company="evagapov">
//   Evgeny Agapov, 2016, CodeGeek audition test project.
// </copyright>
// <summary>
//   Defines the IDoor.cs type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionOOP.Core
{
    /// <summary>
    /// The Door interface.
    /// </summary>
    public interface IDoor
    {
        /// <summary>
        /// Gets the number.
        /// </summary>
        int Number { get; }

        /// <summary>
        /// Opens the door.
        /// </summary>
        void Open();
    }
}
