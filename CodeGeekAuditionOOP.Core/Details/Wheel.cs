﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Wheel.cs" company="evagapov">
//   Evgeny Agapov, 2016, CodeGeek audition test project.
// </copyright>
// <summary>
//   Defines the Wheel.cs type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionOOP.Core.Details
{
    using System.Collections.ObjectModel;
    using System.Linq;

    /// <summary>
    /// The wheel.
    /// </summary>
    public class Wheel : Detail, IRotatable
    {
        /// <summary>
        /// Gets the details encapsulated by current one.
        /// </summary>
        public override sealed ObservableCollection<Detail> Details { get; protected set; }

        /// <summary>
        /// Gets or sets the current number.
        /// </summary>
        private static int CurrentNumber { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Wheel"/> class.
        /// </summary>
        /// <param name="nutsCount">
        /// The nuts count.
        /// </param>
        public Wheel(int nutsCount = 6)
        {
            this.Number = ++CurrentNumber;
            this.Name = $"Колесо №{this.Number}";
            this.Weight = Constants.NutWeight;

            this.Details = new ObservableCollection<Detail>(Enumerable.Range(0, nutsCount).Select(i => new Nut()));
        }

        /// <summary>
        /// Moves the rotatable wheel.
        /// </summary>
        public void Move()
        {
            Output.WriteLine($"Колесо №{this.Number} вращается");
        }
    }
}
