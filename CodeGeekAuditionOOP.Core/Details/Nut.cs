﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Nut.cs" company="evagapov">
//   Evgeny Agapov, 2016, CodeGeek audition test project.
// </copyright>
// <summary>
//   Defines the Nut.cs type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionOOP.Core.Details
{
    /// <summary>
    /// The nut.
    /// </summary>
    public class Nut : Detail
    {
        /// <summary>
        /// Gets or sets the current number.
        /// </summary>
        private static int CurrentNumber { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Nut"/> class.
        /// </summary>
        public Nut()
        {
            this.Number = ++CurrentNumber;
            this.Name = $"Гайка №{this.Number}";
            this.Weight = Constants.NutWeight;
        }
    }
}
