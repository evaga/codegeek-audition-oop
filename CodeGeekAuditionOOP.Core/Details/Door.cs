﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Door.cs" company="evagapov">
//   Evgeny Agapov, 2016, CodeGeek audition test project.
// </copyright>
// <summary>
//   Defines the Door.cs type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionOOP.Core.Details
{
    /// <summary>
    /// The door.
    /// </summary>
    public class Door : Detail, IDoor
    {
        /// <summary>
        /// Gets or sets the current number.
        /// </summary>
        private static int CurrentNumber { get; set; }

        /// <summary>
        /// Gets or sets the value indicating whether the door is open.
        /// </summary>
        private bool IsOpen { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Door"/> class.
        /// </summary>
        public Door()
        {
            this.Number = ++CurrentNumber;
            this.Name = $"Дверь №{this.Number}";
            this.Weight = Constants.DoorWeight;
        }

        /// <summary>
        /// Opens the door.
        /// </summary>
        public void Open()
        {
            this.IsOpen = !this.IsOpen;

            var state = this.IsOpen ? "открыта" : "закрыта";
            Output.WriteLine($"Дсерь №{this.Number} {state}");
        }
    }
}
