﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Detail.cs" company="evagapov">
//   Evgeny Agapov, 2016, CodeGeek audition test project.
// </copyright>
// <summary>
//   Defines the Detail.cs type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionOOP.Core.Details
{
    using System.Linq;

    /// <summary>
    /// The detail.
    /// </summary>
    public abstract class Detail : DetailsCollector
    {
        /// <summary>
        /// Gets or sets the weight.
        /// </summary>
        public float Weight { get; protected set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        public int Number { get; protected set; }

        /// <summary>
        /// Gets the weight of detail including encapsulated details.
        /// </summary>
        /// <returns>
        /// The <see cref="int"/> the weight of detail including encapsulated details.
        /// </returns>
        public float GetSumWeight()
        {
            return this.Weight + this.Details.Sum(detail => detail.GetSumWeight());
        }
    }
}
