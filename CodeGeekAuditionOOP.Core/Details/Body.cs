﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Body.cs" company="evagapov">
//   Evgeny Agapov, 2016, CodeGeek audition test project.
// </copyright>
// <summary>
//   Defines the Body.cs type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionOOP.Core.Details
{
    /// <summary>
    /// The body.
    /// </summary>
    public class Body : Detail, IDoor, IRotatable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Body"/> class.
        /// </summary>
        public Body()
        {
            this.Number = 0;
            this.Name = $"Рама №{this.Number}";
            this.Weight = Constants.BodyWeight;
        }

        /// <summary>
        /// Opens the door.
        /// </summary>
        public void Open()
        {
            Output.WriteLine(@"Увы, это не дверь");
        }

        /// <summary>
        /// Moves the rotatable object.
        /// </summary>
        public void Move()
        {
            Output.WriteLine(@"Машина едет");
        }
    }
}
