﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Constants.cs" company="evagapov">
//   Evgeny Agapov, 2016, CodeGeek audition test project.
// </copyright>
// <summary>
//   Defines the Constants.cs type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionOOP.Core
{
    /// <summary>
    /// The constants.
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// The body weight.
        /// </summary>
        public const float BodyWeight = 500;

        /// <summary>
        /// The door weight.
        /// </summary>
        public const float DoorWeight = 50;

        /// <summary>
        /// The wheel weight.
        /// </summary>
        public const float WheelWeight = 20;

        /// <summary>
        /// The nut weight.
        /// </summary>
        public const float NutWeight = 0.5f;
    }
}
