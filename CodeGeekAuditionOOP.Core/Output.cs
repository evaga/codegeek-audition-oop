﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Output.cs" company="evagapov">
//   Evgeny Agapov, 2016, CodeGeek audition test project.
// </copyright>
// <summary>
//   Defines the Output.cs type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionOOP.Core
{
    using System;

    /// <summary>
    /// The output.
    /// </summary>
    public class Output
    {
        /// <summary>
        /// The written.
        /// </summary>
        public event EventHandler<StringEventAgrs> Written;

        /// <summary>
        /// The output instance.
        /// </summary>
        private static Output instance;

        /// <summary>
        /// Gets the output instance.
        /// </summary>
        public static Output Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Output();
                }

                return instance;
            }
        }

        /// <summary>
        /// Writes line to the output.
        /// </summary>
        /// <param name="line">
        /// The line to be written to output.
        /// </param>
        public static void WriteLine(string line)
        {
            Instance.OnWritten($"{line}\n");
        }

        /// <summary>
        /// The <code>OnWritten</code> event.
        /// </summary>
        /// <param name="message"></param>
        protected virtual void OnWritten(string message)
        {
            this.Written?.Invoke(this, new StringEventAgrs(message));
        }

        /// <summary>
        /// Message event args class.
        /// </summary>
        public class StringEventAgrs : EventArgs
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="StringEventAgrs"/> class.
            /// </summary>
            /// <param name="message">
            /// The message.
            /// </param>
            public StringEventAgrs(string message)
            {
                this.Message = message;
            }

            /// <summary>
            /// Gets or sets the message.
            /// </summary>
            public string Message { get; set; }
        }
    }
}