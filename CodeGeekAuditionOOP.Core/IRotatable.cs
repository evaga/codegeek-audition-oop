﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRotatable.cs" company="evagapov">
//   Evgeny Agapov, 2016, CodeGeek audition test project.
// </copyright>
// <summary>
//   Defines the IRotatable.cs type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionOOP.Core
{
    /// <summary>
    /// The Rotatable interface.
    /// </summary>
    public interface IRotatable
    {
        /// <summary>
        /// Moves the rotatable object.
        /// </summary>
        void Move();
    }
}
